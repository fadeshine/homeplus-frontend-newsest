import React from 'react';
import TaskStatusTag from '../../../components/TaskStatusTag';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { shallow } from 'enzyme';

describe('../../../components/TaskStatusTag', () => {
  it('should render TaskStatusTag by default', () => {
    const wrapper = shallow(<TaskStatusTag  type="testing" />);
    expect(wrapper.find(Chip));
    expect(wrapper.find(Stack));
  });
});