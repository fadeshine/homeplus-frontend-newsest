import styled from 'styled-components';

export const ResetBox = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 400px;
  left: 50%;
  transform: translateX(-50%);
  margin-top: 150px;
  button {
    margin-right: 0;
    margin-top: 1rem;
  }
  h3 {
    width: 100%;
    text-align: center;
  }
`;
