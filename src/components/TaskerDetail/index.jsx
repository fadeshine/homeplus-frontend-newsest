import React from 'react';
import PropTypes from 'prop-types';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Summery from '../TaskerDetailItems/Summery';
import Rates from '../TaskerDetailItems/Rates';
import Reviews from '../TaskerDetailItems/Reviews';
import Introduction from '../TaskerDetailItems/Introduction';
import ratingCalculator from './utils/RatingCalculate';

const TaskerDetail = ({ tasker, taskerReviews }) => {
  const styledBox = {
    display: 'flex',
    margin: '50px',
    '@media (max-width: 445px)': {
      flexDirection: 'column',
      justifyContent: 'center',
      margin: '50px 50px 0',
    },
  };
  return (
    <Card sx={{ display: 'flex', flexDirection: 'column', width: '1152px', marginBottom: '20px' }}>
      <Box sx={styledBox}>
        <Summery tasker={tasker} />
        <Rates rating={!!taskerReviews.length ? ratingCalculator(taskerReviews) : {}} />
      </Box>
      <Introduction tasker={tasker} />
      <Box sx={{ display: 'flex', flexDirection: 'column', marginLeft: '50px' }}>
        <Reviews reviews={!!taskerReviews.length ? taskerReviews : []} />
      </Box>
    </Card>
  );
};

TaskerDetail.propTypes = {
  tasker: PropTypes.object.isRequired,
  taskerReviews: PropTypes.array.isRequired,
};

export default TaskerDetail;
