import React from 'react';
import { Box, Container, Content } from './Footer.style.jsx';
import Discover from '../FooterItems/Discover';
import Company from '../FooterItems/Company';
import PopularLocations from '../FooterItems/PopularLocations';
import PopularCategories from '../FooterItems/PopularCategories';
import ExisitingMembers from '../FooterItems/ExisitingMembers';
import CopyRight from '../FooterItems/CopyRight';

const Footer = () => (
  <Box>
    <Content>
      <Container>
        <Discover />
        <Company />
        <ExisitingMembers />
        <PopularCategories />
        <PopularLocations />
      </Container>
    </Content>

    <CopyRight />
  </Box>
);
export default Footer;
