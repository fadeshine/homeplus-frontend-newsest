import { styled } from '@mui/system';

export const Card = styled('div')({
  width: '80%',
  marginTop: 10,
  padding: 10,
  borderRadius: 5,
  color: '#444',
});

export const Section = styled('div')({
  display: 'inline-block',
  width: '85%',
  alignItems: 'center',
  backgroundColor: '#F2f2f2',
  justifyContent: 'space-between',
  borderRadius: 10,
  padding: '1%',
  marginLeft: 50,
  marginTop: 10,
  '.timeStamp': {
    fontSize: 12,
    paddingRight: 10,
    textAlign: 'end',
  },
});

export const User = styled('div')({
  display: 'flex',
  alignItems: 'center',
  maxHeight: 30,

  '& span': {
    padding: 10,
    paddingLeft: 15,
    fontWeight: 400,
  },
});

export const Message = styled('p')({
  wordWrap: 'break-word',
  marginTop: 15,
  marginLeft: 25,
  lineHeight: 1.4,
  fontWeight: 400,
});
