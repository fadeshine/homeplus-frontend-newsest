import styled from 'styled-components';

export const Card = styled.div`
  width: 93%;
  display: flex;
  border-radius: 10px;
  padding: 1rem;
  flex-direction: column;
  border-style: solid;
  border-width: 2px;
  background-color: white;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1), 0 6px 6px rgba(0, 0, 0, 0.15);
  cursor: pointer;
  :hover {
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  }
  h2,
  p {
    font-weight: 400;
    margin: 0;
    margin-top: 5px;
  }

  .cardContent {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    p {
      font-size: 1.3rem;
      margin: 0px;
      margin-top: 0.3rem;
    }
    .title {
      margin-top: 0.5rem;
      margin-bottom: 1rem;
      font-size: 1.1rem;
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      width: 80%;
    }
  }

  .others {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .taskStatus {
    display: flex;
    gap: 10px;
  }

  @media (max-width: 768px) {
    width: 87%;
    .taskStatus {
      display: flex;
      flex-direction: column;
      gap: 10px;
      align-items: end;
    }
  }
`;
