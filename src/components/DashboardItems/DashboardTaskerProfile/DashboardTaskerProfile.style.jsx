import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 85%;
  display: flex;
  flex-direction: column;
  line-height: 1.6;
  padding: 3%;
  margin: 10px;
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.23), 0 6px 6px rgba(0, 0, 0, 0.25);
`;

export const LineWrapper = styled.div`
  height: 40px;
  width: 100%;
  padding-top: 10px;
  display: flex;
  text-align: left;
`;

export const Label = styled.div`
  width: 200px;
  font-size: 18px;
  font-weight: 500;
`;

export const CategoryLabel = styled(Label)`
  padding-top: 15px;
`;

export const TextArea = styled.div`
  padding: 2%;
  min-height: 2rem;
  width: 96%;
  font-size: 16px;
`;
