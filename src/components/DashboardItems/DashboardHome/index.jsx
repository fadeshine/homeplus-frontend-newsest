import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TaskCalendar from '../TaskCalendar';

export default function DashboardHome({ tasksOfUser, tasksOfTasker }) {
  return (
    <Box sx={{ width: '90%' }}>
      <TaskCalendar tasksOfUser={tasksOfUser} tasksOfTasker={tasksOfTasker} />
    </Box>
  );
}

DashboardHome.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};
