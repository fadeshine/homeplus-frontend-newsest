import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import CancelIcon from '@mui/icons-material/Cancel';
import CustomTextButton from './CustomTextButton';
import TaskCard from '../../TaskCard';
import NoTask from '../DashboadDisableInfo/NoTask';
import TaskDetailCard from '../../TaskDetailCard';
import useComment from '../../../hooks/useComment';
import useOffer from '../../../hooks/useOffer';
import { Display } from './DashboardPostedTasks.style';

const DashboardPostedTasks = ({ tasksOfUser, tasksOfTasker }) => {
  const displayRef = React.useRef(null);
  const currentUser = useSelector((state) => state.currentUser);
  const { commentsRequest, comments } = useComment();
  const { offersRequest, offers } = useOffer();
  const [displayTasks, setDisplayTasks] = useState();
  const [status, setStatus] = useState(null);
  const [displayTask, setTask] = useState(null);
  const [isResetTask, setRestTask] = useState(false);

  const chooseTask = useCallback((task) => {
    setTask(task);
    setRestTask(true);
  }, []);

  const handleStatus = (s) => {
    setStatus(s);
  };

  const CloseDetail = () => {
    setTask(null);
  };

  useEffect(() => {
    if (isResetTask) {
      commentsRequest(displayTask.id);
      offersRequest(displayTask.id);

      setRestTask(false);
    }
    if (tasksOfUser) {
      if (displayTasks !== tasksOfUser) {
        setDisplayTasks(tasksOfUser);
      }
    } else if (tasksOfTasker) {
      if (displayTasks !== tasksOfTasker) {
        setDisplayTasks(tasksOfTasker);
      }
    }
  }, [displayTasks, tasksOfUser, tasksOfTasker, commentsRequest, offersRequest, displayTask, isResetTask]);

  React.useEffect(() => {
    function handleClickOutside(event) {
      if (displayRef.current && !displayRef.current.contains(event.target)) {
        setTask(null);
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [displayRef]);

  const tasksToDisplay = useCallback(
    (tasks) => {
      let filteredTasks = Object.values(tasks);

      const followingTasks = currentUser.followedTasks.map((task) => task.id);
      filteredTasks = filteredTasks.map((task) => {
        return { ...task, following: followingTasks.includes(task.id) };
      });

      if (status) {
        return filteredTasks.filter((task) => task.task_status.includes(status));
      }

      return filteredTasks;
    },
    [status, currentUser]
  );

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '90%' }}>
      {!_.isEmpty(displayTasks) ? (
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '97%',
            alignItems: 'center',
            padding: '10px',
          }}
        >
          <CustomTextButton onClick={() => handleStatus('')}>All</CustomTextButton>
          <CustomTextButton onClick={() => handleStatus('open')}>Open</CustomTextButton>
          <CustomTextButton onClick={() => handleStatus('assigned')}>Assigned</CustomTextButton>
          <CustomTextButton onClick={() => handleStatus('completed')}>Completed</CustomTextButton>
        </Box>
      ) : null}
      <Box
        sx={{
          marginTop: '30px',
          maxWidth: '100%',
          minHeight: '100px',
          paddingBottom: 3,
          display: 'inline-flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexWrap: 'wrap',
          gap: 3,
          overflow: 'scroll',
        }}
      >
        {!_.isEmpty(displayTasks) ? (
          tasksToDisplay(displayTasks)?.map((task) => (
            <TaskCard key={task.id} details={task} onClick={() => chooseTask(task)} />
          ))
        ) : (
          <div style={{ position: 'relative', top: '-100px' }}>
            <NoTask />
          </div>
        )}
        {displayTask && (
          <Display ref={displayRef}>
            <div className="details">
              <TaskDetailCard
                details={displayTask}
                setTask={chooseTask}
                taskComments={comments ? comments : []}
                taskOffers={offers ? offers : []}
              />
              <div className="closeButton" onClick={CloseDetail}>
                <CancelIcon sx={{ fontSize: 30, color: '#444' }} />
              </div>
            </div>
          </Display>
        )}
      </Box>
    </Box>
  );
};

DashboardPostedTasks.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};

export default DashboardPostedTasks;
