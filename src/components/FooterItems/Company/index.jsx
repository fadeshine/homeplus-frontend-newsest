import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Link from '@mui/material/Link';
import { WebLinkItem, Title, MobileLinkItem, TitleIcon } from './Company.style.jsx';

const Company = () => {
  const [isExtant, setIsExtant] = React.useState(true);

  const handleClick = () => {
    setIsExtant(!isExtant);
  };
  return (
    <List>
      <ListItemButton onClick={handleClick} sx={{ justifyContent: 'space-between', padding: '0', margin: '0' }}>
        <Title>Company</Title>
        <TitleIcon>{isExtant ? <AddIcon sx={{ color: '#fff' }} /> : <RemoveIcon sx={{ color: '#fff' }} />}</TitleIcon>
      </ListItemButton>

      <Collapse in={!isExtant} timeout="auto" unmountOnExit>
        <MobileLinkItem>
          <Link href="#">About us</Link>
          <Link href="#">Careers</Link>
          <Link href="#">Media enquiries</Link>
          <Link href="#">Community guidelines</Link>
          <Link href="#">Tasker principles</Link>
          <Link href="#">Terms & conditions</Link>
          <Link href="#">Blog</Link>
          <Link href="#">Contact us</Link>
          <Link href="#">Privacy policy</Link>
          <Link href="#">Investors</Link>
        </MobileLinkItem>
      </Collapse>
      <WebLinkItem>
        <Link href="#">About us</Link>
        <Link href="#">Careers</Link>
        <Link href="#">Media enquiries</Link>
        <Link href="#">Community guidelines</Link>
        <Link href="#">Tasker principles</Link>
        <Link href="#">Terms & conditions</Link>
        <Link href="#">Blog</Link>
        <Link href="#">Contact us</Link>
        <Link href="#">Privacy policy</Link>
        <Link href="#">Investors</Link>
      </WebLinkItem>
    </List>
  );
};
export default Company;
