import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 6vh;
  position: absolute;
  width: 100%;
  font-family: 'Poppins', sans-serif;
  left: 0;
  background-color: #fdfdfd;
  a {
    text-decoration: none;
  }
  a:-webkit-any-link {
    color: #444;
  }
`;
